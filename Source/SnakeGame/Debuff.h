// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "TimerManager.h"
#include "Debuff.generated.h"

UCLASS()
class SNAKEGAME_API ADebuff : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADebuff();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	UFUNCTION()
		float getRandomNumber(float min, float max)
	{
		static const float fraction = 1.0 / (static_cast<float>(RAND_MAX) + 1.0);
		// ���������� ������������ ��������� ����� � ����� ���������
		return static_cast<float>(rand() * fraction * (max - min + 1) + min);
	}
};
