// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Debuff.h"
#include "Interactable.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameGameModeBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MoveSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP; 
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveSpeed);
	AddSnakeElement(2);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewFTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewFTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

	}
	
}


void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractebleInterface = Cast<IInteractable>(Other);
		if (InteractebleInterface)
		{
			InteractebleInterface->Interact(this, bIsFirst);
		}
		AFood* Foody = Cast<AFood>(Other);
		ADebuff* Debuffy = Cast<ADebuff>(Other);
		if (Other == Foody)
		{
			AGameModeBase* GameMode = UGameplayStatics::GetGameMode(GetWorld());
			ASnakeGameGameModeBase* GameModeR = Cast<ASnakeGameGameModeBase>(GameMode);
			if (IsValid(GameModeR))
			{
				GameModeR->SpawnFood();
			}
		}
		else if (Other == Debuffy)
		{
			AGameModeBase* GameMode = UGameplayStatics::GetGameMode(GetWorld());
			ASnakeGameGameModeBase* GameModeR = Cast<ASnakeGameGameModeBase>(GameMode);
			if (IsValid(GameModeR))
			{
				GameModeR->SpawnDebuff();
				GetWorld()->GetTimerManager().SetTimer(DebuffTimerHandle, this, &ASnakeBase::NormalSpeed, 5.0f, false);
			}
		}
		else
		{
			Destroy();
		}
	}
}

void ASnakeBase::NormalSpeed()
{
	SetActorTickInterval(MoveSpeed);
	GetWorld()->GetTimerManager().ClearTimer(DebuffTimerHandle);
}



