// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine/EngineTypes.h"
#include "SnakeGameGameModeBase.generated.h"


class AFood;
class ADebuff;

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY()
		AFood* FoodActor;
	UPROPERTY()
		ADebuff* DebuffActor;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ADebuff> DebuffClass;
	UPROPERTY()
		ESpawnActorCollisionHandlingMethod SpawnCollisionHandlingOverride;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION()
		void SpawnFood();
	UFUNCTION()
		void SpawnDebuff();
	UFUNCTION()
		float getRandomNumber(float min, float max)
	{
		static const float fraction = 1.0 / (static_cast<float>(RAND_MAX) + 1.0);
		// ���������� ������������ ��������� ����� � ����� ���������
		return static_cast<float>(rand() * fraction * (max - min + 1) + min);
	}
};
