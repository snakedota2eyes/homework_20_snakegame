// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Food.h"
#include "Debuff.h"

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
	SpawnDebuff();
}

void ASnakeGameGameModeBase::SpawnFood()
{
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
	FVector SpawnFoodLocation(getRandomNumber(-980, 980), getRandomNumber(-2150, 2150), 45);
	FTransform NewFTransform(SpawnFoodLocation);
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, NewFTransform, params);
	if (FoodActor == 0)
	{
		SpawnFood();
	}
}

void ASnakeGameGameModeBase::SpawnDebuff()
{
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
	FVector SpawnFoodLocation(getRandomNumber(-980, 980), getRandomNumber(-2150, 2150), 45);
	FTransform NewFTransform(SpawnFoodLocation);
	DebuffActor = GetWorld()->SpawnActor<ADebuff>(DebuffClass, NewFTransform, params);
	if (DebuffActor == 0)
	{
		SpawnDebuff();
	}
}